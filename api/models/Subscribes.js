const mongoose = require('mongoose');
var SubscribesSchema = mongoose.Schema({
    email: String,
    cid: String
});
mongoose.model('Subscribes', SubscribesSchema);
module.exports = mongoose.model('Subscribes');