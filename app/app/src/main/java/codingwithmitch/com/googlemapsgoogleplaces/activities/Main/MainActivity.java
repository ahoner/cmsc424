package codingwithmitch.com.googlemapsgoogleplaces.activities.Main;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import codingwithmitch.com.googlemapsgoogleplaces.AlarmReceiver;
import codingwithmitch.com.googlemapsgoogleplaces.RestFetchUser;
import codingwithmitch.com.googlemapsgoogleplaces.activities.Map.MapActivity;
import codingwithmitch.com.googlemapsgoogleplaces.R;
import codingwithmitch.com.googlemapsgoogleplaces.activities.Register.RegisterActivity;
import codingwithmitch.com.googlemapsgoogleplaces.models.User;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = "MainActivity";
    public final static int ALARM_ID = 9999;


    private Button login,register;
   private EditText mName, mPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (Button) findViewById(R.id.Login);
        register = (Button) findViewById(R.id.Register);
        mName = findViewById(R.id.User_ID);
        mPassword = findViewById(R.id.Password);
    }

    public void onStart(){
        super.onStart();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mName.getText().toString();
                String password = mPassword.getText().toString();

                RestFetchUser userJSON = new RestFetchUser(name, password);
                userJSON.execute();
                try {
                    JSONObject data = userJSON.get();
                    if(data != null){
                        User user = new User(data);
                        Intent intent = new Intent(MainActivity.this, MapActivity.class);
                        intent.putExtra("user", user);
                        startActivity(intent);
                        Log.i(TAG, "onClick: you have logged in");
                        Toast.makeText(getApplicationContext(), "You have logged In", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Wrong User Name/ Password", Toast.LENGTH_SHORT).show();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
            }
        });
    }

    public void onPause() {
        super.onPause();

        Intent intentAlarm = new Intent(this, AlarmReceiver.class);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

        PendingIntent pi = PendingIntent.getBroadcast(this, ALARM_ID, intentAlarm, 0);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, pi);
    }

}






















