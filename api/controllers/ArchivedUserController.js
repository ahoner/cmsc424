const ArchivedUser = require('../models/ArchivedUser');

/*
ArchivedUser.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        ArchivedUser.findByIdAndDelete(cat._id, function(err, doc) {});
    });
});
*/

exports.user_create = function(user) {
    console.log(user);
    ArchivedUser.create({
        active: user.active,
        _id: user._id,
        name: user.name,
        email: user.email,
        password: user.password,
        coord: user.coord,
        isPub: user.isPub
    }, function(err, archivedUser) {
        console.log(archivedUser);
    });
};

exports.user_list = function(req, res) {
    console.log(req.body);
    ArchivedUser.find({}, function(err, archivedUser) {
        if (err)
            return res.status(500).send("There was a problem finding the user");
        res.status(200).send(archivedUser);
    })
};