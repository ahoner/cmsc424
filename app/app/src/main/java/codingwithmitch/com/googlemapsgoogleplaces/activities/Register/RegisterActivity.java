package codingwithmitch.com.googlemapsgoogleplaces.activities.Register;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import codingwithmitch.com.googlemapsgoogleplaces.R;
import codingwithmitch.com.googlemapsgoogleplaces.RestRegister;
import codingwithmitch.com.googlemapsgoogleplaces.activities.Main.MainActivity;
import codingwithmitch.com.googlemapsgoogleplaces.activities.Map.MapActivity;
import codingwithmitch.com.googlemapsgoogleplaces.models.User;

//Not sure why AppCompatActivity is extended here but it is in MainActivity.java so I put it here again
public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    //Changed from 9001 to 9002 - figured it needed to be different than MainActivity.java
    private static final int ERROR_DIALOG_REQUEST = 9002;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_page);
            if(isServicesOK()) {
                init();
            }
    }

    private void init(){
        Button next = (Button) findViewById(R.id.Next);
        final Switch isPubSwitch = (Switch) findViewById(R.id.isPub);
        final EditText mName = findViewById(R.id.User_Name);
        final EditText mEmail = findViewById(R.id.User_Email);
        final EditText mPassword = findViewById(R.id.Password);
        final EditText mCoord = findViewById(R.id.Coord);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mName.getText().toString();
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();
                String coord = mCoord.getText().toString();
                boolean isPub = isPubSwitch.isChecked();

                if (password.equalsIgnoreCase(coord)) {

                    RestRegister restRegister = new RestRegister(name, email, password, coord, isPub);

                    restRegister.execute();

                    try {
                        JSONObject data = restRegister.get();
                        if (data != null) {
                            User user = new User(data);
                            Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                            intent.putExtra("user", user);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Please Check Your Email (It Might End Up In Spam Please Check)", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please Check Your Email (It Might End Up In Spam Please Check)", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Please Check If Password are The Same",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(RegisterActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(RegisterActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
