package codingwithmitch.com.googlemapsgoogleplaces;

import android.os.AsyncTask;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class RestFetchUser extends AsyncTask<String, Integer, JSONObject> {

    // Change this depending on where the server is running
    // http://mallard.cf:3000/news/readers/%s - This is the ip for Alex's computer and will sparatically be on
    // http://localhost:3000/news/readers/%s - Use this ip if you are hosting the REST server on your machine and are
    // Emulating the app on it as well (I think, I haven't tried it yet
    // http://(local ip of server):3000/news/readers/%s - Use this if the app is on a physical phone on the same network as
    // as the node server
    private static final String MALLARD_API = "http://mallard.cf:3000/news/users/%s";
    String email, password;
    public RestFetchUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        try {
            // construct url to fetch specific user
            String api_url = String.format(MALLARD_API, email);
            URL url = new URL(api_url);

            StringBuilder postData = new StringBuilder();
            postData.append(URLEncoder.encode("password", "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(this.password), "UTF-8"));
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String response = new String();
            for (String line; (line = ((BufferedReader) in).readLine()) != null; response += line);
            in.close();
            System.out.println(response);
            JSONObject data = new JSONObject(response.toString());
            return data;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}