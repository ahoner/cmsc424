import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PasswordValidator } from './validators/password.validator';
import { HttpClient } from '@angular/common/http';
import { UserService} from '../services/user.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  return: any = '';
  loginFailed = false;
  loginValidationMessages = {
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    'password': [
      { type: 'required', message: 'Password is required'},
    ]
  };
  registerValidationMessages = {
    'firstName': [
      {type: 'required', message: 'First name is required'},
    ],
    'lastName': [
      {type: 'required', message: 'Last name is required'},
    ],
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' }
    ],
    'password': [
      {type: 'required', message: 'Password is required' },
    ],
    'passwordConfirm': [
      {type: 'required', message: 'Confirm password is required'},
      {type: 'areEqual', message: 'Passwords do not match'}
    ]
  };

  loginFormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  });

  registerFormGroup = this.fb.group({
    registerFirstName: ['', Validators.required],
    registerLastName: ['', Validators.required],
    registerEmail: ['', [Validators.required, Validators.email]],
    registerPasswordGroup: this.fb.group({
      registerPassword: ['', Validators.required],
      registerPasswordConfirm: ['', Validators.required]
    }, {validator: PasswordValidator.areEqual})
  });

  onSubmitLogin() {
    this.route.queryParams
      .subscribe(params => this.return = params['return'] || '/about');
    if (this.loginFormGroup.valid) {
      const email = this.loginFormGroup.value.email;
      const password = {password: this.loginFormGroup.value.password};
      this.http.post('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/users/' + email, password).subscribe(data => {
        const tmp: any = data;
        if (tmp && tmp.isPub) {
          this.userService.setState(tmp);
          this.loginFailed = false;
          this.router.navigateByUrl(this.return);
        }
      }, (err) => {
        console.log(err);
        this.loginFailed = true;
      });
    }
  }

  onSubmitRegister() {
    if (this.registerFormGroup.valid) {
      const body = {
        name: this.registerFormGroup.value.registerFirstName + ' ' + this.registerFormGroup.value.registerLastName,
        email: this.registerFormGroup.value.registerEmail,
        password: this.registerFormGroup.get('registerPasswordGroup').value.registerPassword,
        coord: '12',
        isPub: true
      };
      alert('We have sent an email to verify your account');
      this.http.post('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/users', body).subscribe(data => {
        console.log(data);
        if (data) {
          this.userService.setState(data);
          this.router.navigateByUrl(this.return);
        }
      });
    }
  }

  constructor(private route: ActivatedRoute, public fb: FormBuilder, private http: HttpClient, private userService: UserService, private router: Router) {}

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      alert('You are already logged in');
      this.router.navigate(['/about']);
    }
  }
}
