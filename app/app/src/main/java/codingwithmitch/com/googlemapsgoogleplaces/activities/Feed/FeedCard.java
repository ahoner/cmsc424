package codingwithmitch.com.googlemapsgoogleplaces.activities.Feed;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import codingwithmitch.com.googlemapsgoogleplaces.R;

public class FeedCard extends AppCompatActivity {

    TextView mCategory,mDate,mTitle,mDescription;
    String category,date,title,description,image_String;
    ImageView mImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_card);

        mCategory = findViewById(R.id.Feed_Card_Category);
        mDate = findViewById(R.id.Feed_Card_Time);
        mTitle = findViewById(R.id.Feed_Card_Title);
        mDescription = findViewById(R.id.Feed_Card_Description);
        mImage = findViewById(R.id.Feed_Card_Image);

        Intent intent = getIntent();

        category = intent.getStringExtra("category");
        date = intent.getStringExtra("time");
        title = intent.getStringExtra("title");
        description = intent.getStringExtra("description");
        image_String = intent.getStringExtra("image");

        mCategory.setText(category);
        mDate.setText(date);
        mTitle.setText(title);
        mDescription.setText(description);

        Bitmap bm = StringToBitMap(image_String);

        mImage.setImageBitmap(bm);
    }


    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }



}
