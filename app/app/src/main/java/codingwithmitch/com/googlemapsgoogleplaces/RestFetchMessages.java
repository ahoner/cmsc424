package codingwithmitch.com.googlemapsgoogleplaces;

import android.os.AsyncTask;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class RestFetchMessages extends AsyncTask<String, Integer, JSONArray> {
    private static final String MALLARD_API = "http://mallard.cf:3000/news/messages";
    @Override
    protected JSONArray doInBackground(String... strings) {
        try {
            URL url = new URL(MALLARD_API);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer json = new StringBuffer();
            String tmp;
            while((tmp = reader.readLine()) != null) {
                System.out.println(tmp);
                json.append(tmp).append("\n");
            }
            reader.close();
            JSONArray data = new JSONArray(json.toString());
            System.out.println(data.toString());
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
