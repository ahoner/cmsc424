import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticRoutingModule } from './static-routing.module';
import { AboutComponent } from './about/about.component';
import { CreditsComponent } from './credits/credits.component';

@NgModule({
  declarations: [AboutComponent, CreditsComponent],
  imports: [
    CommonModule,
    StaticRoutingModule
  ]
})
export class StaticModule { }
