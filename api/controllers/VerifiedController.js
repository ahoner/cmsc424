const Verified = require('../models/Verified');
const User = require('../models/User');

/*
Verified.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        Verified.findByIdAndDelete(cat._id, function(err, doc) {});
    });
});
*/

exports.verify_user = function(req, res) {
    console.log(req.query.hash);
    Verified.findOne({hash: req.query.hash}, function(err, verified) {
        if (err)
            return res.status(500).send("THere was a problem verifying");
        else {
            console.log(verified);
            User.findOne({email: verified.email}, function(err, user) {
                if (err)
                    return res.status(500).send("There was a problem finding the user");
                else {
                    console.log(user);
                    user.active = true;
                    user.save(function() {
                        if (err) {
                            console.log(err);
                            return res.status(500).send("There was a problem updating the user");
                        }
                        res.send("Account has been successfully verified!<br><a href=\"http://mallard.cf:4200/login\">Back to News</a>");
                    })
                }
            })
        }
    });
};

exports.verify_list = function(req, res) {
    console.log(req.body);
    Verified.find({}, function(err, verify) {
        if (err)
            return res.status(500).send("There was a problem");
        res.status(200).send(verify)
    })
};

exports.verified_create = function(hash, email) {
    Verified.create({
        hash: hash,
        email: email
    },
    function (err, verified) {
        if (err) {
            console.log(err);
        }
    });
};

