import {Component} from '@angular/core';
import { routeAnimations } from './core';
import {UserService} from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimations]
})
export class AppComponent {

  constructor(private router: Router, private userService: UserService) {}

  navigation = [
    { link: 'about', label: 'About'},
    { link: 'login', label: 'Sign In'},
    { link: 'compose', label: 'Compose Article'},
    { link: 'archive', label: 'Archive' },
    { link: 'credits', label: 'Credits'}
  ];

  signOut() {
    this.userService.signOut();
    this.router.navigate(['/about']);
  }
}
