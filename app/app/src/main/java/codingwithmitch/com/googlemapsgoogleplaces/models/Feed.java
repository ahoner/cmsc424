package codingwithmitch.com.googlemapsgoogleplaces.models;

import java.util.Date;

public class Feed {
    private String categories;
    private Date date;
    private Double lat;
    private Double lng;
    private String title;
    private String description;
    private String image;


    public Feed(String categories, Date date, Double lat, Double lng, String title, String description, String image) {
        this.categories = categories;
        this.date = date;
        this.lat = lat;
        this.lng = lng;
        this.title = title;
        this.description = description;
        this.image=image;
    }

    public Feed(){

    }

    public String getCategories() {
        return categories.toLowerCase();
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Feed{" +
                "categories='" + categories + '\'' +
                ", date=" + date +
                ", lat=" + lat +
                ", lng=" + lng +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
