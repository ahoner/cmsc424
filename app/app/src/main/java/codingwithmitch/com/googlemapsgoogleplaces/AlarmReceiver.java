package codingwithmitch.com.googlemapsgoogleplaces;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.Toast;

import codingwithmitch.com.googlemapsgoogleplaces.activities.Main.MainActivity;

public class AlarmReceiver extends BroadcastReceiver {
    int mNotificationId = 001;
    private String channelID = "my_channel";
    Context mContext;
    NotificationManager notificationManager;


    public AlarmReceiver() {
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        // Gets an instance of the NotificationManager service
        mContext = context;

        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(context, mNotificationId, resultIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews contentView = new RemoteViews(
                mContext.getPackageName(),
                R.layout.custom_notification);

        contentView.setTextViewText(R.id.text,"come back");

        createNotificationChannel();

        Notification.Builder notificationBuilder =
                new Notification.Builder(mContext, channelID);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setContentIntent(resultPendingIntent);
        notificationBuilder.setContent(contentView);

        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationId, notificationBuilder.build());
    }

    private void createNotificationChannel()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // TODO: Create Notification Channel with id channelID,
            // name R.string.channel_name
            // and description R.string.channel_description of high importance
            CharSequence name = mContext.getString(R.string.channel_name);
            String description = mContext.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel mChannel = new NotificationChannel(channelID, name, importance);
            mChannel.setDescription(description);

            notificationManager =
                    (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);

        }
    }



}