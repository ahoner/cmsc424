const express = require('express');
const router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended : true}));
router.use(bodyParser.json());

const messageController = require('../controllers/MessageController');
const userController = require('../controllers/UserController');
const categoryController = require('../controllers/CategoryController');
const verifiedController = require('../controllers/VerifiedController');
const hasController = require('../controllers/HasController');
const archivedMessageController = require('../controllers/ArchivedMessageController');
const archivedUserController = require('../controllers/ArchivedUserController');

/// MESSAGE ROUTES ///

// POST request for creating message
router.post('/messages', messageController.message_create);

// GET request for get all messages
router.get('/messages', messageController.message_list);

// GET request for get message by id
router.get('/messages/:id', messageController.message_detail);

// GET request for get user by message id
router.get('/messages/:id/user', messageController.message_user);

//  PUT request for update message by id
router.put('/messages/:id', messageController.message_update);

// DELETE request for delete message by id
router.delete('/messages/:id', messageController.message_delete);

/// USER ROUTES ///

// POST request for creating user
router.post('/users', userController.user_create);

// GET request for get all users
router.get('/users', userController.user_list);

// PUT request for get user by id
router.post('/users/:id', userController.user_detail);

// PUT request for update user by id
router.put('/users/:id', userController.user_update);

// DELETE request for delete user by id
router.delete('/users/:id', userController.user_delete);

/// CATEGORY ROUTES ///
// GET request for get all users
router.get('/categories', categoryController.category_list);

router.post('/categories', categoryController.category_create);

/// VERIFIED ROUTS ///
/// GET request  to verify
router.get('/verify', verifiedController.verify_user);

/// HAS ROUTES ///
/// GET request to has
router.get('/has', hasController.has_list);

router.post('/has', hasController.has_create);

/// ARCHIVEDMESSAGES ROUTES ///
/// GET request archived messages ///
router.get('/archivedMessages', archivedMessageController.message_list);

/// ARCHIVED USERS ROUTES ///
/// GET request archived users ///
router.get('/archivedUsers', archivedUserController.user_list);

module.exports = router;
