const Message = require('../models/Message');
const User = require('../models/User');

/*
Message.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        Message.findByIdAndDelete(cat._id, function(err, doc) {});
    });
});
*/

exports.message_create = function(req, res) {
    console.log(req.body);
    Message.create({
            pubID: req.body.pubID,
            creationTime: req.body.creationTime,
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            lat: req.body.lat,
            lng: req.body.lng,
            range: req.body.range,
            title: req.body.title,
            content: req.body.content,
            imgName: req.body.imgName,
            imageContentType: req.body.imageContentType,
            imageContent: req.body.imageContent
        },
        function (err, message) {
            if (err)
                return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(message);
        });
};

exports.message_list = function(req, res) {
    console.log(req.body);
    Message.find({}, function(err, messages) {
        if (err)
            return res.status(500).send("There was a problem finding the messages.");
        res.status(200).send(messages);
    });
};

exports.message_detail = function (req, res) {
    console.log(req.body);
    Message.findById(req.params.id, function(err, message) {
        if (err)
            return res.status(500).send("There was a problem finding the message");
        if (!message)
            return res.status(404).send("No message found.");
        res.status(200).send(message);
    });
};

exports.message_user = function (req, res) {
    console.log(req.body);
    Message.findById(req.params.id, function(err, message) {
        if (err)
            return res.status(500).send("There was a problem finding the message");
        if (!message)
            return res.status(404).send("No message found.");

        User.findById(message.pubID, function(err, user) {
            if (err)
                return res.status(500).send("There was a problem finding the user");
            if (!user)
                return res.status(404).send("No user found (That's weird).");
            res.status(200).send(user);
        });
    });
};

exports.message_delete = function(req, res) {
    console.log(req.body);
    Message.findByIdAndRemove(req.params.id, function (err, message) {
        if (err)
            return res.status(500).send("There was a problem deleting the message.");
        res.status(200).send("Message " + message.id + " was deleted.");
    })
};

exports.message_update = function(req, res) {
    console.log(req.body);
    Message.findByIdAndUpdate(req.params.id, req.body, {new: true}, function(err, message) {
        if (err)
            return res.status(500).send("There was a problem updating the message.");
        res.status(200).send(message);
    })
};
