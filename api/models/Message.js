const mongoose = require('mongoose');
var MessageSchema = new mongoose.Schema({
    pubID: String,
    creationTime: String,
    startTime: String,
    endTime: String,
    lat: Number,
    lng: Number,
    range: Number,
    title: String,
    content: String,
    imgName: String,
    imageContentType: String,
    imageContent: String
});

mongoose.model('Message', MessageSchema);
module.exports = mongoose.model('Message');