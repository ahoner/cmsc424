const Has = require('../models/Has');

/*
Has.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        Has.findByIdAndDelete(cat._id, function(err, doc) {});
    })
});
*/

exports.has_create = function(req, res) {
    console.log(req.body);
    Has.create({
        id: req.body.id,
        cid: req.body.cid
    },
    function (err, has) {
        if (err)
            return res.status(500).send("There was a problem adding the info to the database");
        res.status(200).send(has);
    })
};

exports.has_list = function(req, res) {
    console.log(req.body);
    Has.find({}, function(err, has) {
        if (err)
            return res.status(500).send("There was a problem finding the has relation.");
        res.status(200).send(has)
    })
};