package codingwithmitch.com.googlemapsgoogleplaces;

import android.os.AsyncTask;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestFetchHas extends AsyncTask<String, Integer, JSONArray> {
    private static final String MALLARD_API = "http://mallard.cf:3000/news/has";
    @Override
    protected JSONArray doInBackground(String... strings) {
        try {
            URL url = new URL(MALLARD_API);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuffer json = new StringBuffer(1024);
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                System.out.println(tmp);
                json.append(tmp).append("\n");
            }
            reader.close();
            JSONArray data = new JSONArray(json.toString());
            System.out.println(data.toString());
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
