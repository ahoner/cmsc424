package codingwithmitch.com.googlemapsgoogleplaces.activities.Feed;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;

import codingwithmitch.com.googlemapsgoogleplaces.R;
import codingwithmitch.com.googlemapsgoogleplaces.RestFetchCategories;
import codingwithmitch.com.googlemapsgoogleplaces.RestFetchHas;
import codingwithmitch.com.googlemapsgoogleplaces.RestFetchMessages;
import codingwithmitch.com.googlemapsgoogleplaces.activities.Map.MapActivity;
import codingwithmitch.com.googlemapsgoogleplaces.models.Feed;
import codingwithmitch.com.googlemapsgoogleplaces.models.SpinnerItem;
import codingwithmitch.com.googlemapsgoogleplaces.models.User;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class FeedActivity extends AppCompatActivity {

    Button mMap, Enter,selectStart,selectEnd;
    Spinner spinner,distanceSpinner;
    TextView startDate,endDate;
    Calendar cStartDate,cEndDate;
    private DatePickerDialog mDatePickerStart, mDatePickerEnd;
    boolean earlyBoundSelected = false;
    Date earlyBound = new Date();
    boolean lateBoundSelected = false;
    Date lateBound = new Date();


    RecyclerView recyclerView;
    ArrayList<Feed> feedList;
    ArrayList<String> checkedItems;
    FeedAdapter mFeedAdapater;
    Date date,late_date;
    double radiusMiles = 50;
    double curr_Lat,curr_Lng,latMax,latMin,lngMax,lngMin;
    private static final String TAG = "FeedActivity";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_page);

        Intent intent = getIntent();

        curr_Lat = intent.getDoubleExtra("curr_lat",0.0);
        curr_Lng = intent.getDoubleExtra("curr_lng",0.0);


        User user;
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            user = bundle.getParcelable("user");
        } else {
            user = null;
        }
        final User finalUser = user;

        mMap = findViewById(R.id.Map);
        Enter = findViewById(R.id.Enter);
        spinner =  findViewById(R.id.spinner);
        selectStart =findViewById(R.id.selectStart);
        selectEnd = findViewById(R.id.selectEnd);
        startDate = findViewById(R.id.startDateText);
        endDate = findViewById(R.id.endDateText);

        calcLatLngMaxMin();

        RestFetchCategories restFetchCategories = new RestFetchCategories();
        restFetchCategories.execute();
        String[] categories = null;
        try {
            JSONArray categoriesData = restFetchCategories.get();
            categories = new String[categoriesData.length()];
            for(int i = 0; i < categoriesData.length(); i++) {
                categories[i] = (String)((JSONObject)categoriesData.get(i)).get("name");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<SpinnerItem> categoryListItems = new ArrayList<>();

        for (int i = 0; i < categories.length; i++) {
            SpinnerItem item = new SpinnerItem();
            item.setItemName(categories[i]);
            item.setSelected(false);
            categoryListItems.add(item);
        }


        final MultiSelectAdapter multiSelectAdapter = new MultiSelectAdapter(this, 0, categoryListItems);
        spinner.setAdapter(multiSelectAdapter);

        recyclerView = findViewById(R.id.recyclerViewMyFeeds);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        feedList = new ArrayList<Feed>();
        Map<String, List<String>> hasMap = new HashMap<>();
        RestFetchMessages restFetchMessages = new RestFetchMessages();
        restFetchMessages.execute();
        RestFetchHas restFetchHas = new RestFetchHas();
        restFetchHas.execute();
        checkedItems = new ArrayList<String>();

        try {
            JSONArray messagesData = restFetchMessages.get();
            JSONArray hasList = restFetchHas.get();
            for (int i = 0; i < hasList.length(); i++) {
                JSONObject relation = (JSONObject) hasList.get(i);
                String id = (String)relation.get("id");
                String cid = (String)relation.get("cid");
                if (hasMap.get(id) == null) {
                    hasMap.put(id, new ArrayList<String>());
                }
                hasMap.get(id).add(cid);

            }

            Date current_date = Calendar.getInstance().getTime();
            Log.i(TAG, "onCreate: current_date "+ current_date);


            for (int i = 0; i < messagesData.length(); i++) {
                Log.i(TAG, "onCreate: here ");
                JSONObject jsonMessage = (JSONObject) messagesData.get(i);
                String pattern = "yyyy/MM/dd";
                SimpleDateFormat sdf = new SimpleDateFormat(pattern);

                Date date = sdf.parse((String)jsonMessage.get("creationTime"));
                Date startDate = sdf.parse((String)jsonMessage.get("startTime"));
                Date endDate = sdf.parse((String)jsonMessage.get("endTime"));
                Log.i(TAG, "startTime "+ startDate);
                Log.i(TAG, "endTime "+ endDate);

                if(current_date.before(endDate) && current_date.after(startDate)) {
                   Log.i(TAG, "onCreate: hererererererererer!!!");
                    Feed feed = new Feed(
                            hasMap.get(jsonMessage.get("_id")).toString(),
                            date,
                            (Double) jsonMessage.get("lat"),
                            (Double) jsonMessage.get("lng"),
                            (String) jsonMessage.get("title"),
                            (String) jsonMessage.get("content"),
                            (String) jsonMessage.get("imageContent"));

                    if (feed.getLat() < latMax && feed.getLat() > latMin &&
                            feed.getLng() < lngMax && feed.getLng() > lngMin) {
                        feedList.add(feed);
                    }
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mFeedAdapater = new FeedAdapter(this,feedList);
        recyclerView.setAdapter(mFeedAdapater);

        mMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FeedActivity.this,MapActivity.class);
                intent.putExtra("user", finalUser);
                startActivity(intent);
            }
        });

        distanceSpinner = findViewById(R.id.distance);
        ArrayAdapter<CharSequence> mArrayAdapter = ArrayAdapter.createFromResource(
                this, R.array.radius_spinner_options, android.R.layout.simple_spinner_item);
        mArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        distanceSpinner.setAdapter(mArrayAdapter);
        distanceSpinner.setSelection(4);
        distanceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch(position){
                    case 0: // 1 miles
                        radiusMiles = 1.0;
                        break;
                    case 1: // 5 miles
                        radiusMiles = 5.0;
                        break;
                    case 2: // 10 mile
                        radiusMiles = 10.0;
                        break;
                    case 3: // 20 mile
                        radiusMiles = 20.0;
                        break;
                    case 4: // 50 mile
                        radiusMiles = 50.0;
                        break;
                    case 5: // 100 mile
                        radiusMiles = 100.0;
                        break;
                    case 6: // 200 mile
                        radiusMiles = 200.0;
                        break;
                    default:
                        Log.i("wtf: ", "How did we get here");
                        break;

                }
                calcLatLngMaxMin();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        selectStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cStartDate = Calendar.getInstance();
                int currMonth = cStartDate.get(Calendar.MONTH);
                int currDay = cStartDate.get(Calendar.DAY_OF_MONTH);
                int currYear = cStartDate.get(Calendar.YEAR);

                mDatePickerStart = new DatePickerDialog(FeedActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        cStartDate.set(year, month, day);
                        String temp =  (month + 1) + "/" + day + "/" + year;
                        startDate.setText(temp);
                        earlyBound = new Date((year-1900), month, day);
                        earlyBoundSelected = true;
                    }
                }, currYear, currMonth, currDay);
                mDatePickerStart.show();
            }
        });

        selectEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cEndDate = Calendar.getInstance();
                int currMonth = cEndDate.get(Calendar.MONTH);
                int currDay = cEndDate.get(Calendar.DAY_OF_MONTH);
                int currYear = cEndDate.get(Calendar.YEAR);

                mDatePickerEnd = new DatePickerDialog(FeedActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        cEndDate.set(year, month, day);
                        String temp = (month + 1) + "/" + day + "/" + year;
                        endDate.setText(temp);
                        lateBound = new Date((year-1900), month, day);
                        lateBoundSelected = true;
                    }
                }, currYear, currMonth, currDay);
                mDatePickerEnd.show();
            }
        });


        Enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkedItems = multiSelectAdapter.getCheckedItems();
                Intent intent = new Intent(FeedActivity.this,SearchResult.class);
                intent.putExtra("checkedlist",checkedItems);
                intent.putExtra("latMax", latMax);
                intent.putExtra("latMin", latMin);
                intent.putExtra("lngMax", lngMax);
                intent.putExtra("lngMin", lngMin);

                intent.putExtra("earlyBoundSelected", (earlyBoundSelected ? 1 : 0));
                intent.putExtra("earlyBound", earlyBound);
                intent.putExtra("lateBoundSelected", (lateBoundSelected ? 1 : 0));
                intent.putExtra("lateBound", lateBound);

                startActivity(intent);
            }
        });



    }

    private void calcLatLngMaxMin() {

        latMax = curr_Lat + (radiusMiles/69.1);
        latMin = curr_Lat - (radiusMiles/69.1);
        lngMax = curr_Lng + (radiusMiles/(69.1*(Math.cos(curr_Lat/57.3))));
        lngMin = curr_Lng - (radiusMiles/(69.1*(Math.cos(curr_Lat/57.3))));

    }

}
