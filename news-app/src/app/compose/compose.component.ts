import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormBuilder, Validators} from '@angular/forms';
import {ROUTE_ANIMATIONS_ELEMENTS} from '../core';
import { UserService } from '../services/user.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.scss']
})
export class ComposeComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  title = 'Create a message';
  selectedTab;
  lat = null;
  lng = null;
  zoom: number;
  imageContentType: String;
  imageContent: String;
  messages: any = [];
  filteredMessages: any = [];
  filterStartDate: Date;
  filterEndDate: Date;
  filterCats: any = [];
  filteredCats: any = [];
  filteredDates: any = [];
  has: any = [];
  markers: Marker[] = [];
  categories: any;
  currentMarker: Marker = {
    lat: null,
    lng: null,
    draggable: true
  };
  addressFormGroup = this.fb.group({
    address: ['']
  });
  messageFormGroup = this.fb.group({
    title: ['', Validators.required],
    content: ['', Validators.required],
    startDate: ['', Validators.required],
    endDate: ['', Validators.required],
    range: ['', Validators.required],
    image: ['', Validators.required],
    categories: ['', Validators.required]
  });
  dateFilterFormGroup = this.fb.group({
    filterStartDate: [''],
    filterEndDate: ['']
  });
  catFormGroup = this.fb.group({
    catName: [''],
  });
  constructor(private userService: UserService, public fb: FormBuilder, private http: HttpClient) {
    this.http = http;
  }

  mapClick($event: any) {
    this.currentMarker.lat = ($event.coords.lat);
    this.currentMarker.lng = ($event.coords.lng);
  }

  markerDragEnd(m: Marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  ngOnInit() {
    this.dateFilterFormGroup.controls['filterStartDate'].valueChanges.subscribe(data => {
      const startDate: any = new Date(data);
      const endDate: any = new Date(this.dateFilterFormGroup.value.filterEndDate);
      if (startDate && endDate && startDate < endDate) {
        this.filterStartDate = startDate;
        this.filterEndDate = endDate;
        this.filterMessages();
      }
    });
    this.dateFilterFormGroup.controls['filterEndDate'].valueChanges.subscribe(data => {
      const startDate: any = new Date(this.dateFilterFormGroup.value.filterStartDate);
      const endDate: any = new Date(data);
      if (startDate && endDate && startDate < endDate) {
        this.filterStartDate = startDate;
        this.filterEndDate = endDate;
        this.filterMessages();
      }
    });
    this.zoom = 9;
    console.log('Current User');
    this.http.get('https://ipinfo.io/geo')
      .subscribe(data => {
        const tmp: any = data;
        const coord = tmp.loc.split(',');
        this.lat = parseFloat(coord[0]);
        this.lng = parseFloat(coord[1]);
        this.currentMarker = {
          lat: this.lat,
          lng: this.lng,
          draggable: true
        };
      });
    this.http.get('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/has')
      .subscribe(data => {
        const tmp: any = data;
        this.has = tmp;
        console.log(this.has);
      });
    this.http.get('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/messages')
      .subscribe(data => {
        const tmp: any = data;
        this.messages = tmp;
        console.log(this.messages);
        for (let i = 0; i < this.messages.length; i++) {
          this.messages[i].categories = [];
          for (let j = 0; j < this.has.length; j++) {
            if (this.messages[i]._id === this.has[j].id) {
              this.messages[i].categories.push(this.has[j].cid);
            }
          }
          this.filteredMessages = this.messages;
          this.markers.push({
            lat: this.messages[i].lat,
            lng: this.messages[i].lng,
            draggable: false
          });
        }
      });
    this.http.get('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/categories')
      .subscribe(data => {
        console.log(data);
        this.categories = data;
      });
  }

  fileChange($event: any) {
    const reader: any = new FileReader();
    if ($event.target.files && $event.target.files.length > 0) {
      const file: any = $event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageContentType = reader.result.split(',')[0];
        this.imageContent = reader.result.split(',')[1];
      };
    }
  }

  onSubmit() {
    const date = formatDate(new Date(), 'yyyy/MM/dd', 'en');
    const categoryString = this.messageFormGroup.value.categories;
    console.log(categoryString);
    console.log(date);
    const email = this.userService.getUser().email;
    console.log(email);
    const startDate = this.messageFormGroup.value.startDate;
    const endDate = this.messageFormGroup.value.endDate;
    const messageData = {
      categories: categoryString,
      pubID: email,
      creationTime: date,
      startTime: startDate.getFullYear() + '/' + (startDate.getMonth() + 1)  + '/' + startDate.getDate(),
      endTime: endDate.getFullYear() + '/' + (endDate.getMonth() + 1) + '/' + endDate.getDate(),
      lat: this.currentMarker.lat,
      lng: this.currentMarker.lng,
      range: this.messageFormGroup.value.range,
      title: this.messageFormGroup.value.title,
      content: this.messageFormGroup.value.content,
      imgName: this.messageFormGroup.value.image,
      imageContentType: this.imageContentType,
      imageContent: this.imageContent
    };
    this.messages.unshift(messageData);
    this.filteredMessages = this.messages;
    this.markers.unshift({
      lat: messageData.lat,
      lng: messageData.lng,
      draggable: false
    });
    console.log(messageData);
    this.selectedTab = 0;
    this.http.post('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/messages', messageData).subscribe(data => {
      const tmp: any = data;
      console.log(tmp);
      const hasData = [];
      for (let i = 0; i < categoryString.length; i++) {
        hasData.push({
          id: tmp._id,
          cid: categoryString[i],
        });
      }
      console.log(categoryString);
      console.log(hasData);
      for (let i = 0; i < hasData.length; i++) {
        console.log(hasData[i]);
        this.http.post('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/has', hasData[i]).subscribe(data1 => {
          console.log(data1);
        });
      }
    });
  }

  onSearch() {
    const address = this.addressFormGroup.value.address;
    const url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCfGgQKImadBxKWABrMSO6T91pgmcGSXdI&address=' + address;
    this.http.get(url).subscribe(data => {
      const tmp: any = data;
      const coords = {
        lat: tmp.results[0].geometry.location.lat,
        lng: tmp.results[0].geometry.location.lng
      };
      this.lat = coords.lat;
      this.lng = coords.lng;
      this.currentMarker.lat = this.lat;
      this.currentMarker.lng = this.lng;
      this.zoom = 14;
    });
  }

  createCategory() {
    const category = {name: this.catFormGroup.value.catName};
    this.categories.unshift(category);
    this.http.post('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/categories', category).subscribe(data => {
      console.log(data);
    });
  }

  focusMarker(index) {
    const message = this.messages[index];
    this.zoom = 14;
    this.lat = message.lat;
    this.lng = message.lng;

  }

  filterCatsEvent($event) {
    console.log($event);
    this.filterCats = $event.value;
    this.filterMessages();
  }

  catFilter(cats: any) {
    this.filteredCats = this.messages.filter((message, index, array) => {
      if (cats.length > 0) {
        return cats.filter((n) => {
          return message.categories.indexOf(n) > -1;
        }).length > 0;
      }
      return true;
    });
  }

  dateFilter(startDate: Date, endDate: Date) {
    if (startDate && endDate) {
      this.filteredDates = this.messages.filter((message, index, array) => {
        const mStartDate = new Date(message.startTime);
        const mEndDate = new Date(message.endTime);
        return (startDate > mStartDate ? startDate : mStartDate) <= (endDate < mEndDate ? endDate : mEndDate);
      });
    }
  }

  filterMessages() {
    this.catFilter(this.filterCats);
    this.dateFilter(this.filterStartDate, this.filterEndDate);
    if (this.filteredDates.length === 0) {
      this.filteredMessages = this.filteredCats;
    } else if (this.filteredMessages.length === 0) {
      this.filteredMessages = this.filteredDates;
    } else {
      this.filteredMessages = this.filteredCats.filter((e) => {
        return this.filteredDates.indexOf(e) > -1;
      });
    }
  }

  tab1() {
    this.selectedTab = 0;
  }

  tab2() {
    this.selectedTab = 1;
  }
}

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
