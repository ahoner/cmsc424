import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.scss']
})
export class ArchiveComponent implements OnInit {

  archivedMessages: any = [];
  archivedUsers: any = [];
  has: any = [];

  constructor(private router: Router, private userService: UserService, private http: HttpClient) { }

  ngOnInit() {
    this.http.get('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/has')
      .subscribe(data => {
        const tmp: any = data;
        this.has = tmp;
        console.log(this.has);
      });
    this.http.get('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/archivedMessages')
      .subscribe(data => {
        const tmp: any = data;
        this.archivedMessages = tmp;
        console.log(this.archivedMessages);
        for (let i = 0; i < this.archivedMessages.length; i++) {
          this.archivedMessages[i].categories = [];
          for (let j = 0; j < this.has.length; j++) {
            if (this.archivedMessages[i]._id === this.has[j].id) {
              this.archivedMessages[i].categories.push(this.has[j].cid);
            }
          }
        }
      });
    this.http.get('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/archivedUsers')
      .subscribe(data => {
        const tmp: any = data;
        this.archivedUsers = tmp;
        console.log(this.archivedUsers);
      });
  }

  deleteAccount() {
    console.log('account deleted pressed');
    console.log('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/users/' + this.userService.getUser().email);
    this.http.delete('https://cors-anywhere.herokuapp.com/http://mallard.cf:3000/news/users/' + this.userService.getUser().email)
      .subscribe(data => {
        const tmp: any = data;
        console.log(tmp);
      });
    this.userService.signOut();
    this.router.navigate(['/about']);
  }
}
