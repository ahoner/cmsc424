package codingwithmitch.com.googlemapsgoogleplaces.activities.Feed;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import codingwithmitch.com.googlemapsgoogleplaces.R;
import codingwithmitch.com.googlemapsgoogleplaces.models.Feed;

public class FeedAdapter extends  RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {

    private Context mCtx;

    private List<Feed> feedList;

    public FeedAdapter(Context mCtx, List<Feed> feedList){
        this.mCtx = mCtx;
        this.feedList = feedList;
    }


    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.feed_layout,null);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        final Feed feed = feedList.get(position);

        holder.title.setText(feed.getTitle());
        holder.category.setText(feed.getCategories());
        holder.description.setText(feed.getDescription());
        holder.time.setText(feed.getDate().toString());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mCtx,FeedCard.class);
                intent.putExtra("title",feed.getTitle());
                intent.putExtra("category",feed.getCategories());
                intent.putExtra("description",feed.getDescription());
                intent.putExtra("time",feed.getDate().toString());
                intent.putExtra("lat",feed.getLat());
                intent.putExtra("lng",feed.getLng());
                intent.putExtra("image",feed.getImage());

                mCtx.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        if(feedList == null){
            return 0;
        }
        return feedList.size();
    }

    public class FeedViewHolder extends RecyclerView.ViewHolder{

        TextView title,category,address,description,time;



        public FeedViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.textViewTitle);
            category = itemView.findViewById(R.id.textViewCategory);
            description = itemView.findViewById(R.id.textViewDescription);
            time = itemView.findViewById(R.id.textViewTime);
        }
    }
}
