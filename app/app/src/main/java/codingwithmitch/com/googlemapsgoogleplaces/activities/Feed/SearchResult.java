package codingwithmitch.com.googlemapsgoogleplaces.activities.Feed;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import codingwithmitch.com.googlemapsgoogleplaces.R;
import codingwithmitch.com.googlemapsgoogleplaces.RestFetchHas;
import codingwithmitch.com.googlemapsgoogleplaces.RestFetchMessages;
import codingwithmitch.com.googlemapsgoogleplaces.models.Feed;

public class SearchResult extends AppCompatActivity {
    ArrayList<Feed> feedList, finalList;
    ArrayList<String> checkedItems;
    RecyclerView recyclerView;
    FeedAdapter mFeedAdapater;
    double latMax,latMin,lngMax,lngMin;
    Date date,late_date;

    boolean isEarlyBoundSelected,isLateBoundSelected;
    Date earlyBound,lateBound;


    private static final String TAG = "SearchResult";
    Time time;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        checkedItems = new ArrayList<String>();
        feedList = new ArrayList<Feed>();
        finalList = new ArrayList<Feed>();



        Intent intent =getIntent();
        checkedItems = intent.getStringArrayListExtra("checkedlist");
        latMax = intent.getDoubleExtra("latMax", 0.0);
        latMin = intent.getDoubleExtra("latMin", 0.0);
        lngMax = intent.getDoubleExtra("lngMax", 0.0);
        lngMin = intent.getDoubleExtra("lngMin", 0.0);
        earlyBound = (Date) intent.getSerializableExtra("earlyBound");
        lateBound = (Date) intent.getSerializableExtra("lateBound");
        isEarlyBoundSelected = intent.getIntExtra("earlyBoundSelected", 0) == 1;
        isLateBoundSelected = intent.getIntExtra("lateBoundSelected", 0) == 1;

        Log.i(TAG, "onCreate1: " + checkedItems.toString());

        recyclerView = findViewById(R.id.recyclerSearchFeeds);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Map<String, List<String>> hasMap = new HashMap<>();
        RestFetchMessages restFetchMessages = new RestFetchMessages();
        restFetchMessages.execute();
        RestFetchHas restFetchHas = new RestFetchHas();
        restFetchHas.execute();
        try {
            JSONArray messagesData = restFetchMessages.get();
            JSONArray hasList = restFetchHas.get();
            for (int i = 0; i < hasList.length(); i++) {
                JSONObject relation = (JSONObject) hasList.get(i);
                String id = (String)relation.get("id");
                String cid = (String)relation.get("cid");
                if (hasMap.get(id) == null) {
                    hasMap.put(id, new ArrayList<String>());
                }
                hasMap.get(id).add(cid);
            }
            for (int i = 0; i < messagesData.length(); i++) {
                JSONObject jsonMessage = (JSONObject) messagesData.get(i);
                String pattern = "yyyy/MM/dd";
                SimpleDateFormat sdf = new SimpleDateFormat(pattern);
                Date date = sdf.parse((String)jsonMessage.get("creationTime"));
                Feed feed = new Feed(
                        hasMap.get(jsonMessage.get("_id")).toString(),
                        date,
                        (Double)jsonMessage.get("lat"),
                        (Double)jsonMessage.get("lng"),
                        (String)jsonMessage.get("title"),
                        (String)jsonMessage.get("content"),
                        (String)jsonMessage.get("imageContent"));
                feedList.add(feed);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        int i = 0;

        for(Feed feed: feedList){
            i++;
            Log.i(TAG, "feed Categories "+ i +" "+ Arrays.toString(feed.getCategories().split(" ")));
            Log.i(TAG, "Checked Items "+ checkedItems.toString());

            String category = feed.getCategories().replaceAll("[^A-Za-z]+"," ");

            String [] categories = category.split(" ");

            for(int j = 0; j < categories.length;j++){
                Log.i(TAG, "categories:  "+j+ " "+ categories[j]);

                if(checkedItems.contains(categories[j])){

                    if(feed.getLat() > latMax || feed.getLat() < latMin ||
                            feed.getLng() > lngMax || feed.getLng() < lngMin){
                        continue;
                    }else if(isEarlyBoundSelected
                            && earlyBound.after(feed.getDate())) {
                        continue;
                    }else if(isLateBoundSelected && lateBound.before(feed.getDate())) {
                        continue;
                    } else {
                        finalList.add(feed);
                        break;
                    }
                }
            }
        }

        Log.i(TAG, "onCreate3: "+ finalList.size());
        mFeedAdapater = new FeedAdapter(this,finalList);
        recyclerView.setAdapter(mFeedAdapater);

    }
}
