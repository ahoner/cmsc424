import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {CreditsComponent} from './static/credits/credits.component';
import {ComposeComponent} from './compose/compose.component';
import {UserService} from './services/user.service';
import {ArchiveComponent} from "./archive/archive.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full'
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'credits',
    component: CreditsComponent
  }, {
    path: 'compose',
    component: ComposeComponent,
    canActivate: [UserService]
  }, {
    path: 'archive',
    component: ArchiveComponent,
    canActivate: [UserService]
  }, {
    path: '**',
    redirectTo: 'about'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
