const Category = require('../models/Category');
var categories = [
    { name: 'SPORTS' },
    { name: 'TECH' },
    { name: 'POP' },
];

/*
Category.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        Category.findByIdAndDelete(cat._id, function(err, doc) {});
    });
});
*/

Category.find({}, function(err, doc) {
    if (doc === undefined || doc.length == 0) {
        Category.insertMany(categories);
    } else {
        categories.forEach(function(cat) {
            if (!doc.some(e => e.name === cat.name)) {
                Category.create(cat);
            }
        })
    }
});

exports.category_create = function(req, res) {
    console.log(req.body);
    Category.create({
        name: req.body.name,
    },
    function (err, category) {
        if (err)
            return res.status(500).send("There was a problem adding the info to the database");
        res.status(200).send(category);
    });
};

exports.category_list = function(req, res) {
    console.log(req.body);
    Category.find({}, function(err, categories) {
        if (err)
            return res.status(500).send("There was a problem finding the categories.");
        res.status(200).send(categories);
    });
};

exports.category_detail = function(req, res) {};

exports.category_delete = function(req, res) {};

exports.message_update = function(req, res) {};