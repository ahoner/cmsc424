package codingwithmitch.com.googlemapsgoogleplaces.models;

public class SpinnerItem {
    private String itemName;
    private boolean selected;

    public String getItemName() {
        return this.itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
