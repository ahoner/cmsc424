const User = require('../models/User');
const VerifiedController = require('./VerifiedController');
const Message = require('../models/Message');
const nodeMailer = require('nodemailer');
const ArchivedMessageController = require('./ArchivedMessageController');
const ArchivedUserController = require('./ArchivedUserController');
const smtpTransport = nodeMailer.createTransport({
    service: "Gmail",
    auth: {
        user: "mallard.cf@gmail.com",
        pass: "4ec7f58d8A*"
    }
});

/*
User.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        User.findByIdAndDelete(cat._id, function(err, doc) {});
    });
});
*/

exports.user_create = function(req, res) {
    console.log(req.body);
    User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        coord: req.body.coord,
        isPub: req.body.isPub
    },
    function (err, user) {
        if (err) {
            console.log(err);
            return res.status(500).send("There was a problem adding the information to the database.");
        }
        var rand = Math.floor((Math.random() * 100) + 54);
        VerifiedController.verified_create(rand, req.body.email);
        var host = req.get('host');
        var link = "http://" + req.get('host') + "/news/verify?hash=" + rand;
        var mailOptions = {
            to : user.email,
            subject: 'Please confirm your Email account',
            html: 'Hello,<br> Please click on the link to verify you email.<br>' + link
        };
        console.log(mailOptions);
        smtpTransport.sendMail(mailOptions, function(error, response) {
            if (error) {
                console.log(error);
                res.end("error");
            } else {
                console.log("Message sent: " + response.message);
                res.end("sent");
            }
        })
    });
};

exports.user_list = function(req, res) {
    console.log(req.body);
    User.find({}, function(err, user) {
        if (err)
            return res.status(500).send("There was a problem finding the user.");
        res.status(200).send(user);
    });
};

exports.user_detail = function (req, res) {
    console.log(req.body);
    User.findOne({email: req.params.id}, function(err, user) {
            if (err)
                return res.status(500).send("There was a problem finding the user");
            if (!user)
                return res.status(404).send("No user found.");
            else {
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (isMatch && user.active) {
                        res.status(200).send(user);
                    } else {
                        let response = {
                            message: "Failed to log in"
                        };
                        res.status(403).send(response);
                    }
                });
            }
    });
};

exports.user_delete = function(req, res) {
    console.log('req.params');
    console.log(req.params);
    User.findOneAndDelete({email: req.params.id}, function (err, user) {
        if (err)
            return res.status(500).send("There was a problem deleting the user.");
        console.log('user');
        console.log(user);
        ArchivedUserController.user_create(user);
        Message.find({ pubID: user.email }, function(err, docs) {
            docs.forEach(function(msg) {
                Message.findByIdAndDelete(msg._id, function(err, message) {
                    console.log('message');
                    console.log(message);
                    ArchivedMessageController.message_create(message);
                })
            })
        })
    })
};

exports.user_update = function(req, res) {
    console.log(req.body);
    User.findOne({email: req.params.id}, function(err, user) {
        user.name = req.body.name;
        user.email = req.body.email;
        user.password = req.body.password;
        user.coord = req.body.coord;
        user.isPub = req.body.isPub;
        user.save(function() {
            if (err) {
                console.log(err);
                return res.status(500).send("There was a problem updating the user.");
            }
            res.status(200).send(user);
        });
    });
};
