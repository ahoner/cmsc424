const express = require('express');
const app = express();
const db = require('./db');

const newsRouter = require('./routes/news');
app.use('/news', newsRouter);

module.exports = app;