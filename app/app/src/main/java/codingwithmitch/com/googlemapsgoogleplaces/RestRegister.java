package codingwithmitch.com.googlemapsgoogleplaces;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.Buffer;
import java.util.LinkedHashMap;
import java.util.Map;

//This class is made for the register page
public class RestRegister extends AsyncTask<String, Integer, JSONObject> {

    // Change this depending on where the server is running
    // http://mallard.cf:3000/news/readers/%s - This is the ip for Alex's computer and will sparatically be on
    // http://localhost:3000/news/readers/%s - Use this ip if you are hosting the REST server on your machine and are
    // Emulating the app on it as well (I think, I haven't tried it yet
    // http://(local ip of server):3000/news/readers/%s - Use this if the app is on a physical phone on the same network as
    // as the node server
    private static final String MALLARD_API = "http://mallard.cf:3000/news/users";
    String name, email, password, coord;
    boolean isPub;

    public RestRegister(String name, String email, String password, String coord, boolean isPub) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.coord = coord;
        this.isPub = isPub;
    }

    //This is copy and pasted from the RestFetchUser code!
    @Override
    protected JSONObject doInBackground(String... strings) {
        try {
            URL url = new URL(MALLARD_API);
            Map<String,Object> params = new LinkedHashMap<>();
            params.put("name", this.name);
            params.put("email", this.email);
            params.put("password", this.password);
            params.put("coord", this.coord);
            params.put("isPub", this.isPub);

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) {
                    postData.append('&');
                }

                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }

            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String response = new String();
            for (String line; (line = ((BufferedReader) in).readLine()) != null; response += line);

            for (int c; (c = in.read()) >= 0;) {
                System.out.print((char) c);
            }

            // If the user account was able to be sent to DB
            return new JSONObject(response);
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
