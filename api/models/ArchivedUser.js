const mongoose = require('mongoose');
var ArchivedUserSchema = new mongoose.Schema({
    name: String,
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    coord: String,
    isPub: Boolean,
    active: {
        type: Boolean,
        default: false
    }
});

mongoose.model('ArchivedUser', ArchivedUserSchema);
module.exports = mongoose.model('ArchivedUser');
