package codingwithmitch.com.googlemapsgoogleplaces.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class User implements Parcelable {

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    private String name;
    private String email;
    private String coord;
    private boolean isPub;

    public User(JSONObject data) throws JSONException {
        this.name = (String) data.get("name");
        this.email = (String) data.get("email");
        this.coord = (String) data.get("coord");
        this.isPub = (boolean) data.get("isPub");
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCoord() {
        return this.coord;
    }

    public void setCoord(String coord) {
        this.coord = coord;
    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.coord = in.readString();
        this.isPub = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.email);
        parcel.writeString(this.coord);
        parcel.writeByte((byte) (this.isPub ? 1 : 0));
    }
}
