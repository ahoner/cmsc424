const mongoose = require('mongoose');
var VerifiedSchema = new mongoose.Schema({
    hash: String,
    email: String
});

mongoose.model('Verified', VerifiedSchema);
module.exports = mongoose.model('Verified');
