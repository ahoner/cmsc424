import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate {

  loggedIn = null;

  constructor(private router: Router) {
    this.loggedIn = JSON.parse(localStorage.getItem('user'));
  }

  setState(data): void {
    this.loggedIn = data;
    localStorage.setItem('user', JSON.stringify(data));
    console.log('Now logged in as ' + data);
  }

  getUser() {
    return this.loggedIn;
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  signOut() {
    this.loggedIn = null;
    localStorage.clear();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/login'], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }

}
