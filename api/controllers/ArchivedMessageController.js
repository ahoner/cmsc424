const ArchivedMessage = require('../models/ArchivedMessage');

/*
ArchivedMessage.find({}, function(err, doc) {
    doc.forEach(function(cat) {
        console.log(cat);
        ArchivedMessage.findByIdAndDelete(cat._id, function(err, doc) {});
    });
});
*/

exports.message_create = function(msg) {
    ArchivedMessage.create({
        _id: msg._id,
        pubID: msg.pubID,
        creationTime: msg.creationTime,
        startTime: msg.startTime,
        endTime: msg.endTime,
        lat: msg.lat,
        lng: msg.lng,
        range: msg.range,
        title: msg.title,
        content: msg.content,
        imgName: msg.imgName,
        imageContentType: msg.imageContentType,
        imageContent: msg.imageContent
    }, function(err, archivedMsg) {
        console.log(archivedMsg);
    })
};

exports.message_list = function(req, res) {
    console.log(req.body);
    ArchivedMessage.find({}, function(err, archivedUser) {
        if (err)
            return res.status(500).send("There was a problem finding the user");
        res.status(200).send(archivedUser);
    })
};
