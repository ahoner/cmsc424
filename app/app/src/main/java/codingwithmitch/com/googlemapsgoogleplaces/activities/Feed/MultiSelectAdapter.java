package codingwithmitch.com.googlemapsgoogleplaces.activities.Feed;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import codingwithmitch.com.googlemapsgoogleplaces.R;
import codingwithmitch.com.googlemapsgoogleplaces.models.SpinnerItem;


public class MultiSelectAdapter extends ArrayAdapter<SpinnerItem> {
    private Context mContext;
    private ArrayList<SpinnerItem> listItems;
    private MultiSelectAdapter adapter;
    private ArrayList<String> checkedItems;
    private boolean isFromView = false;

    private static final String TAG = "MultiSelectAdapter";


    public MultiSelectAdapter(Context context, int resource, List<SpinnerItem> objects) {
        super(context, resource, objects);
        checkedItems = new ArrayList<String >();
        this.mContext = context;
        this.listItems = (ArrayList<SpinnerItem>) objects;
        this.adapter = this;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listItems.get(position).getItemName());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listItems.get(position).isSelected());
        isFromView = false;

   /*    if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
            holder.mTextView.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
            holder.mTextView.setVisibility(View.VISIBLE);
       }*/


        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                if (isChecked) {
                    SpinnerItem item = listItems.get(getPosition);
                    String itemName= item.getItemName();
                    checkedItems.add(itemName.toLowerCase());
                    Log.i(TAG, "onCheckedChanged: in" + itemName);
                    Log.i(TAG, "onCheckedChanged: " + checkedItems.toString());
                }else{
                    SpinnerItem item = listItems.get(getPosition);
                    String itemName= item.getItemName();
                    checkedItems.remove(itemName.toLowerCase());
                    Log.i(TAG, "onCheckedChanged: out" + itemName);
                    Log.i(TAG, "onCheckedChanged: "+ checkedItems.toString());
                }

            }
        });
        return convertView;
    }

    public ArrayList<String> getCheckedItems() {
        return checkedItems;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}
